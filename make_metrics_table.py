import json
from mako.template import Template

metrics = {}
with open("metrics.json","r") as metrics:
    metrics = json.load(metrics)

metrics_list = list(metrics.values())

for metric in metrics_list:
    tierfield = "tier"
    if tierfield not in metric:
        tierfield = "tiers"
    metric[tierfield] = ", ".join(metric[tierfield])
    if metric[tierfield] == "premium, ultimate":
        metric[tierfield] = "premium"
    elif "free" in metric[tierfield]:
        metric[tierfield] = "free"
    elif metric[tierfield] == "ultimate":
        metric[tierfield] = "ultimate"
    if "performance_indicator_type" in metric:
        metric["performance_indicator_type"] = ", ".join(metric["performance_indicator_type"])

mytemplate = Template(filename='template/index.html')

fields = ["key_path","data_category","description","product_section","product_stage","product_group","product_category","value_type","status","time_frame","tier","performance_indicator_type","milestone"]

with open("public/index.html","w") as outfile:
    outfile.write(mytemplate.render(metrics = metrics_list, fields = fields))